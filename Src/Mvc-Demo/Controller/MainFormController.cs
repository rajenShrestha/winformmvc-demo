﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/9/2015 11:14:07 PM
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rs.WinForm.Mvc.Aplication.Services;

namespace Rs.WinForm.Mvc.Aplication.Controller
{
    public class MainFormController : IFormController
    {
        private IMainView _mainView;
        private IRepositoryService _repositoryService;

        public MainFormController(IMainView mainView, IRepositoryService repositoryService)
        {
            _mainView = mainView;
            _repositoryService = repositoryService;
            _mainView.SetController(this);
            this.UpdateGrid();
        }

        public void Add(Models.Employee employee)
        {
            _repositoryService.Add(employee);
            _mainView.UpdateGridView(employee);
        }

        public void UpdateGrid()
        {
            var employees = _repositoryService.GetAll();
            _mainView.UpdateGridView(employees);
        }
    }
}
