﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rs.WinForm.Mvc.Aplication.Controller;
using Rs.WinForm.Mvc.Aplication.Services;

namespace Rs.WinForm.Mvc.Aplication
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainForm mainFrom = new MainForm();

            IRepositoryService repositoryService = new MemoryRespositoryService();
            IFormController formController = new MainFormController(mainFrom, repositoryService);

            Application.Run(mainFrom);
        }
    }
}
