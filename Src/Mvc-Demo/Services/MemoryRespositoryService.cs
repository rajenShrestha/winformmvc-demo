﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/10/2015 9:18:38 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rs.WinForm.Mvc.Aplication.Models;

namespace Rs.WinForm.Mvc.Aplication.Services
{
    public class MemoryRespositoryService:IRepositoryService
    {
        public List<Models.Employee> GetAll()
        {
            var employees = new List<Employee>();
            employees.AddRange(list);
            return employees;
        }

        public bool Add(Models.Employee employee)
        {
            int id = list.Last().Id + 1;
            employee.Id = id;
            list.Add(employee);
            return true;
        }

        private  List<Employee> list = new List<Employee>() {
            new Employee(){Id=1, Age=28, Gender="M", LastName="Shrestha", Name="Rajen"},
            new Employee(){Id=2, Age=28, Gender="F", LastName="Shrestha", Name="Suzana"},
        };
    }
}
