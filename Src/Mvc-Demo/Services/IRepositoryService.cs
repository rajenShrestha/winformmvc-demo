﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/10/2015 9:17:05 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rs.WinForm.Mvc.Aplication.Models;

namespace Rs.WinForm.Mvc.Aplication.Services
{
    public interface IRepositoryService
    {
        List<Employee> GetAll();
        bool Add(Employee employee);
    }
}
