﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 3/9/2015 11:21:42 PM
*/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rs.WinForm.Mvc.Aplication.Models
{
    public class Employee
    {
        public static readonly string COLUMN_Id = "Id";
        public static readonly string COLUMN_NAME = "Name";
        public static readonly string COLUMN_LASTNAME = "LastName";
        public static readonly string COLUMN_GENDER = "Gender";
        public static readonly string COLUMN_AGE = "Age";

        public int Id { get; set; }
        [System.ComponentModel.DisplayName("First Name")]      
        public string Name { get; set; }
        [System.ComponentModel.DisplayName("Last Name")]
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
    }
}
