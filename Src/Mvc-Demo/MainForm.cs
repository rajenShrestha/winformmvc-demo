﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rs.WinForm.Mvc.Aplication.Controller;
using Rs.WinForm.Mvc.Aplication.Models;

namespace Rs.WinForm.Mvc.Aplication
{
    public partial class MainForm : Form, IMainView
    {
        private IFormController _controller;
        private BindingSource _bindingSource;

        public MainForm()
        {
            InitializeComponent();           
            InitialiseControls();
        }

        private void InitialiseControls()
        {
            _bindingSource = new BindingSource();
            dataGridViewEmplyees.DataSource = _bindingSource;
            dataGridViewEmplyees.EditMode = DataGridViewEditMode.EditProgrammatically;            
            buttonSave.Enabled = false;
        }

        public void UpdateGridView(List<Models.Employee> employees)
        {
            if (_bindingSource.DataSource == null)
            {
                _bindingSource.DataSource = employees;
            }
            else
            {
                employees.ForEach( e => _bindingSource.Add(e));                
            }
            //Hide Id column. this line of code has to be here.
            HideIdColumnInGridView();
        }

        public void UpdateGridView(Models.Employee employee)
        {
            _bindingSource.Add(employee);
        }

        public void SetController(IFormController controller)
        {
            _controller = controller;
        }

        private void DataGridViewEmplyees_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int selectedIndex = e.RowIndex;

            DataGridViewCellCollection cells = dataGridViewEmplyees.Rows[selectedIndex].Cells;
            var employee = new Employee()
            {
                Id = (int)cells[Employee.COLUMN_Id].Value,
                Age = (int)cells[Employee.COLUMN_AGE].Value,
                Gender = cells[Employee.COLUMN_GENDER].Value as string,
                LastName = cells[Employee.COLUMN_LASTNAME].Value as string,
                Name = cells[Employee.COLUMN_NAME].Value as string
            };

            PopulatedTextBoxes(employee);

        }

        private void PopulatedTextBoxes(Employee employee)
        {
            labelIdHidden.Text = employee.Id.ToString();
            textBoxName.Text = employee.Name;
            textBoxLastName.Text = employee.LastName;
            textBoxGender.Text = employee.Gender;
            numericUpDownAge.Value = employee.Age;
        }

        private void ClearTextBoxes()
        {
            labelIdHidden.Text = "0";
            textBoxName.Text = string.Empty;
            textBoxLastName.Text = string.Empty;
            textBoxGender.Text = string.Empty;
            numericUpDownAge.Value = 0;
        }

        private void HideIdColumnInGridView()
        {
            dataGridViewEmplyees.Columns["Id"].Visible = false;
        }

        private void ToolStripMenuItemNew_Click(object sender, EventArgs e)
        {
            ClearTextBoxes();
            buttonSave.Enabled = true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            //Check if the form value is correct
            var employee = CreateEmployee();
            _controller.Add(employee);
        }

        /// <summary>
        /// Creates Employee object from text boxes
        /// </summary>
        /// <returns></returns>
        private Employee CreateEmployee()
        {
            var employee = new Employee();
            employee.Name = textBoxName.Text;
            employee.LastName = textBoxLastName.Text;
            employee.Gender = textBoxGender.Text;
            employee.Age = (int)numericUpDownAge.Value;

            return employee;
        }

    }
}
